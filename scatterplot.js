!function() {
  /* A function to draw a scatterplot using d3.js into an html 
     element. Optionally specify width, height, and radius.
     Optionally log data to javascript console.
  
    usage: scatter.draw(<target element>,<data>,width,height,radius,logging)
  
     Defaults: width: 600; height: 400; radius: 3; logging: off
  */
  var scatter = {
    version: "1.2"
  };
  this.scatter = scatter;
  
  // Default values
  var w = 600;
  var h = 400;
  var radius = 3;
  var log = false;
  var padding = 50;  // pad away from the edges when scaling
  var clickFunc =  function(d) {console.log(d);};
  var dataArray=false;
  
  scatter.drawData = function(dataObject) {
        scatter.initPlot(dataObject);
        scatter.drawPlot(dataObject);
        scatter.drawLegend(dataObject);
  };
  
  scatter.draw = function(target,source,callback,width,height,rad,logging) {  // Draw from a single CSV or array of objects
    var elem = (typeof target === 'string') ? target : "body"; // optional target HTML element
    if (typeof width === 'number') w = width; // optional SVG width
    if (typeof height === 'number') h = height;  // optional SVG height
    if (typeof rad === 'number') radius = rad; // optional radius
    if (typeof logging === 'boolean') log = logging;
    if (typeof callback === 'function') clickFunc = callback;
  
    // Variables needed by all functions 
    var svg,g,clip;
    var xScale,yScale,cScale;
    var xAxis,yAxis; 
  
    // Get data
    if (typeof source === "string") {     // data is in a csv file
      d3.csv(source, function(error,data) {
        if (error) {      // error is only set if something went wrong reading the file.
          console.log(error);
        } else {
          if (log) {console.log(data);}
        }
        dataArray = false;
        scatter.initSVG(elem);
        var dataset = data;
        scatter.drawData(dataset);
      });
    } else { // data is an array of objects
      if (Array.isArray(source)) {
        dataArray = true;
        scatter.initSVG(elem);
        scatter.drawData(source);
      }
      else {alert("Source '"+source+"' not understood.\nYou must provide an array of objects or a path to a CSV!");}
  
    };
  };
  
  scatter.initSVG = function(elem) {    // Initialize SVG
    svg = d3.select(elem)
                .append("svg")
                .attr("width",w)
                .attr("height",h);
    clip = svg.append("clipPath") // create a clipping path
     .attr("id","chartClip")
     .append("rect")
     .attr("x",padding)
     .attr("y",padding)
     .attr("width",w-padding*2)
     .attr("height",h-padding*2);
    g = svg.append("g").attr("clip-path","url(#chartClip)"); 
  };
  
  scatter.initPlot = function(dataObject) {
    // Scales
    xScale = d3.scale.linear().domain(d3.extent(dataObject, function(d) { return parseFloat(d.x);}));
    xScale.range([padding, w-padding]);
    yScale = d3.scale.linear().domain(d3.extent(dataObject, function(d) { return parseFloat(d.y);}));
    yScale.range([h-padding , padding]);
    cScale = d3.scale.category10().domain(d3.extent(dataObject, function(d) { return d.class;}));
  
    // Axes
    xAxis = d3.svg.axis()
                      .scale(xScale)
                      .orient("bottom");
    yAxis = d3.svg.axis()
                      .scale(yScale)
                      .orient("left");
  
    // Zoom
    var zoom = d3.behavior.zoom()
                 .translate([0,0])
                 .scale(1)
                 .scaleExtent([0.5,4])
                 .x(xScale)
                 .y(yScale)
                 .on("zoom",zoomed);
    function zoomed() {
      if (!d3.event.sourceEvent) {return;}
      var tar = d3.select(d3.event.sourceEvent.target);
      var t = d3.event.translate;  // needed to reverse-transform the clipping path
      var s = d3.event.scale;   // needed to reverse-transform the clipping path
      tar.select("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
      tar.select("#chartClip").select("rect").attr("transform","scale(" + 1/s + ")").attr("x", padding-t[0]).attr("y", padding-t[1]);
      tar.select(".x.axis").call(xAxis);
      tar.select(".y.axis").call(yAxis);
    }
    svg.call(zoom).call(zoom.event);
  };
  
  scatter.drawPlot = function(dataObject) {     // Draw the scatterplot
    var circles = g.selectAll("circle")
       .data(dataObject);

    circles.enter()   // Add new circles for unbound data
        .append("circle")
        .attr("r",radius);

    // Plot the data
    circles.attr("cx",function(d) {
              return xScale(parseFloat(d.x));
              })
           .attr("cy",function(d) {
               return yScale(parseFloat(d.y));
               })
           .attr("fill",function(d) {
               return cScale(d.class);
               })
           .attr("data-legend",function(d) {return d.class;})
           .append("svg:title")
           .text(function(d,i) { 
                  if (dataArray) {
                    return d.id ? d.id : "element "+i; 
                  } else {
                    return d.id ? d.id : "line "+(i+2); // Add 2 b/c of header line and i[0]
                  }
                }); 

    circles.on("click",function(d){clickFunc(d);});

    // Draw the axes
    svg.append("g").call(xAxis).attr("class","x axis").attr("transform","translate(0,"+ (h-padding) +")");
    svg.append("g").call(yAxis).attr("class","y axis").attr("transform","translate("+padding+",0)");
  };
  
  scatter.drawLegend = function(dataObject) {
      // Get the list of classes 
      classes = d3.nest().key(function(d) {return d.class;}).entries(dataObject);
      // Get the longest class for size & placement
      textlength = d3.max(classes, function(d) {return d.key.length;});

      var legend = svg.append("g").attr("class","legend");
      legend.attr("transform","translate("+(w-(Math.max(padding,textlength*8)))+","+(padding)+")");  // left edge of legend is in at least padding, but can grow to accomodate class 
      legend.selectAll("rect")
            .data(classes)
            .enter()
            .append("circle")
            .attr("cx",radius+8)
            .attr("cy",function(d,i) {return i*20+2;})
            .attr("r",radius)
            .attr("fill",function(d) {
                return cScale(d.key);
                })
      legend.selectAll("text")
            .data(classes)
            .enter()
            .append("text")
            .attr("x",radius+18)
            .attr("y",function(d,i) {return i*20+5;})
            .text(function(d) {return d.key;});
      legend.append("rect").attr("class","legend").attr("x",0).attr("y",-10).attr("width",Math.max(padding-8,radius*2+18+textlength*5)).attr("height",10+(classes.length*20));
  };

}();
